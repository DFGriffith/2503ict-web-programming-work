@extends('product.layout')

@section('title') 
    Create
@stop 

@section('content') 
    
{{ Form::open(array('action' => 'ProductController@store')) }} 
{{ Form::label('name', 'Product Name: ') }}
{{ Form::text('name') }}
{{ $errors->first('name') }}
<p></p>
{{ Form::label('price', 'Price: ') }} 
{{ Form::text('price') }}
{{ $errors->first('price') }}
<p></p>
{{ Form::submit('Create') }} 
{{ Form::close() }}
    
@stop