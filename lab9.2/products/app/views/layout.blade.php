
<!DOCTYPE html>

<html>
<head>
  <title>@yield('title')</title>

</head>
<body>
@if (Auth::check())
    {{ Auth::user()->username }} {{ link_to_route('user.logout', "(Sign out)") }}
@else
    {{ Form::open( array('url' => secure_url('user/login')) ) }}
      {{ Form::label('username', 'User Name: ')  }}
      {{ Form::text('username') }}
      {{ $errors->first('username') }}
      {{ Form::label('password', 'Password: ')  }}
      {{ Form::password('password') }}
      {{ $errors->first('password') }}
      {{ Form::submit('Sign in') }}
    {{ Form::close() }}
@endif

  
@section('content')
Default content
@show
  </body>
</html>