@extends('layout')

@section('title') 
    Create
@stop 

@section('content') 
    {{ Form::model( $product, array('method' => 'PUT', 'route' => array('product.update', $product->id )) ) }}
    
    {{ Form::label('name', 'Product Name: ') }}
    {{ Form::text('name') }}
    <p></p>
    {{ Form::label('price', 'Price: ') }} 
    {{ Form::text('price') }}
    <p></p>
    {{ Form::submit('Create') }} 
    {{ Form::close() }}
    
@stop