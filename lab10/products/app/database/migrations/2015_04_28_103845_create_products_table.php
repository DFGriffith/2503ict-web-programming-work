<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// create pruducts table
		Schema::create('products', function($table)
		{
			$table->increments('id'); 
			$table->string('name'); 
			$table->float('price');
          	$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// drop products table
		Schema::drop('products');
	}

}
