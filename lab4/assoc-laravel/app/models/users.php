<?php
/* Library users.  Data as of 27 March 2015. */
function getUsers()
{
  $users = array(
      array('name' => 'Gough Whitlam', 'address' => '123 Fake str', 'phoneNo' => '123456789', 'email' => 'g_whit@hotmail.com'),
      array('name' => 'Malcolm Fraser', 'address' => 'Roswell Area 51', 'phoneNo' => '987654321', 'email' => 'mal@live.com.au'),
      array('name' => 'Bob Hawke', 'address' => '13 Elm str', 'phoneNo' => '131008', 'email' => 'bhawke@gmail.com'),
      array('name' => 'Paul Keating', 'address' => '10050 Cielo Drive', 'phoneNo' => '1300DOMINOS', 'email' => 'keats@gmail.com'),
      array('name' => 'John Howard', 'address' => '333 Wonder View Avenue', 'phoneNo' => '1300PIZZAHUT', 'email' => 'howdogs@hotmail.com'),
      array('name' => 'Kevin Rudd', 'address' => '20601 Bohemian Avenue', 'phoneNo' => '911', 'email' => 'turd@toilet.com'),
      array('name' => 'Julia Gillard', 'address' => '6 Illuminatus Way', 'phoneNo' => '000', 'email' => 'sacubus@illuminatus.nwo')
  );
  return $users;
}
?>

