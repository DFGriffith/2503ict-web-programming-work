 <?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/users.php";


// Display search form
Route::get('/', function()
{
	return View::make('user.query');
});

// Perform search and display results
Route::get('search', function()
{
  $query = Input::get('query');

  $results = search($query);

	return View::make('user.results')->withUsers($results)->withQuery($query);
});


/* Functions for PM database example. */

/* Search sample data for $name or $year or $state from form. */
function search($query) {
  $users = getUsers();
  
  if (!empty($query)) {
	$results = array();
	foreach ($users as $user) {
	    if ( (stripos($user['name'], $query) !== FALSE) || (strpos($user['address'], $query) !== FALSE 
	    || strpos($user['phoneNo'], $query) !== FALSE) || (stripos($user['email'], $query) !== FALSE)) 
	    {
		    $results[] = $user;
	    }
	}
	$users = $results;
    }

  return $users;
}