@extends('layouts.master')

@section('title')
Associative array search results page
@stop

@section('content')

<h2>Results for '{{{ $query }}}'</h2>
<h3>Results</h3>

@if (count($users) == 0)

<p>No results found.</p>

@else 

<table class="bordered">
<thead>
<tr><th>Name</th><th>Address</th><th>Phone Number</th><th>E-Mail</th></tr>
</thead>
<tbody>

@foreach($users as $user)
  <tr><td>{{{ $user['name'] }}}</td><td>{{{ $user['address'] }}}</td><td>{{{ $user['phoneNo'] }}}</td><td>{{{ $user['email'] }}}</td></tr>
@endforeach

</tbody>
</table>
@endif

<p><a href="{{ secure_url('/') }}">New search</a></p>
@stop