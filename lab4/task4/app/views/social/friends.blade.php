@extends('layouts.master')

@section('post')
<h2>YOUR FRIENDS: <br> 0 friends</h2>
@stop

@section('content')
You have no friends!

<br><p class='link'><a href= {{ secure_url('/') }}>Return Home</a></p>
@stop