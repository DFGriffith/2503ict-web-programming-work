@extends('layouts.master')


@section('title')
	Social Network - Home
@stop


@section('content')
	<div class='row'>
      <!--Col 1-->
      <div class='col-xs-8'>
        <!--Post form-->
        <form class="postForm" action="createPost" method="post" enctype="multipart/form-data">
          <b>Name:</b> <br>
          	<input class="form-control" type="text" name="name" size="30" placeholder="Enter your name"/>
          <b>Title:</b>   <br>
         	 <input class="form-control" type="text" name="title" size="40" placeholder="Enter a title"/>
          <b>Image:</b>
         	 <input type="file" name="fileToUpload" id="fileToUpload"/>
          <b>Message:</b> <br>
          	<textarea class="form-control" rows='3' cols='74' name="message" placeholder="Enter your message"></textarea> <br>
        	<div class="formButtons">
            <input class="btn btn-default" type="submit" value="Post" name="submit">
            <input class="btn btn-default" type="reset" value="Reset">
          </div>
        </form>
        
        <!--Post timeline-->
        <div class="postTimeline">
          
          @if (count($posts) == 0)
          <p>You have no posts!</p>
          
          @else 
          @foreach( array_reverse($posts) as $post)
            <div class="post">
              <img class='photo' src='images/spock.jpg' alt='spock' > 
              <b>Name:</b> {{{ $post->User_name }}} <br> 
              <b>Title:</b> {{{ $post->Title }}} <br> 
              <b>Message:</b> {{{ $post->Message }}} <br>
              <b>Number of comments:</b> {{{ $post->Comment_count }}} <br>
              <a href="{{{ url("view_comments/$post->Id") }}}">View Comments</a> <br>
              <a href="{{{ url("edit_post/$post->Id") }}}">Edit Post</a> <br>
              <a href="{{{ url("del_post/$post->Id") }}}">Delete Post</a>
            </div>
          @endforeach

          @endif
          
        </div>
        
      </div>
      
      <!--Col 2 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="images/ads/heinz.jpg"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="images/ads/smoking.jpg"></img>
        </div>
      </div>
      
    </div>
@stop