@extends('layouts.master')


@section('title')
	Social Network - Comments
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-8'>
        <!--Post-->
        <div class="comPost">
          <img class='photo' src='images/spock.jpg' alt='spock' > 
          <b>Name:</b> {{{ $post->User_name }}} <br> 
          <b>Title:</b> {{{ $post->Title }}} <br> 
          <b>Message:</b> {{{ $post->Message }}} <br>
          <b>Number of comments:</b> {{{ $post->Comment_count }}}<br>
        </div>
        
        <!--Comment form-->
        <form class="commentForm" action="createComment" method="post">
          <input type="hidden" name="id" value="{{{ $post->Id }}}"> 
          <b>Name:</b> <br>
          	<input class="form-control" type="text" name="name" size="30" placeholder="Enter your name"/>
          <b>Comment:</b> <br>
          	<textarea class="form-control" rows='3' cols='74' name="comment" placeholder="Enter a comment"></textarea> <br>
        	<div class="formButtons">
            <input class="btn btn-default" type="submit" value="Post" name="submit">
            <input class="btn btn-default" type="reset" value="Reset">
          </div>
        </form>
        
        <!--Comment timeline-->
        <div class="commentTimeline">
          
          @if (count($comments) == 0)
          <p>You have no Comments!</p>
          
          @else 
          @foreach($comments as $comment)
            <div class="comment">
              <b>Name:</b> {{{ $comment->User_name }}} <br> 
              <b>Message:</b> {{{ $comment->Message }}} <br>
              <a href="{{{ url("del_comment/$comment->Id/$post->Id") }}}">Delete Comment</a> <br>
            </div>
          @endforeach
          @endif
        </div>
        
      </div>
      
    <!--Col 2 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="images/ads/heinz.jpg"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="images/ads/smoking.jpg"></img>
        </div>
      </div>
      
    </div>
@stop