@extends('layouts.master')


@section('title')
	Social Network - Documentation
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-12'>
        <div class='docBody'>
          <h3>Site Documentation:</h3>
          <p>For this submission I have provided an input section for an image within the 	
          post form, although it is currently non-functional. At this point in the project no 	
          images will be stored within the database, and a generic substitute image will 	
          be used for all posts. </p>
          <p>I was sadly not able to complete one component of the assignment completely. After hours of unsuccessful research, I was 
          still encountering a problem routing to the view comments page. Routing from the home page will display the page but without
          the image and the ability to add comments, but routing from the Edit post form with identical code will load the page 
          perfectly.</p>
        
          <h3>Site Diagram:</h3> 
          <img id='docImg1' src='doc/sitediagram.png' alt='Site Diagram'> <br>
          
          <h3>Entity-Relationship Diagram:</h3>
          <img id='docImg2' src='doc/erdiagram.png' alt='Database ER Diagram'>
        </div>
      </div>
      
  </div>
@stop