@extends('layouts.master')


@section('title')
	Social Network - Edit Post
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-8'>
        <!--Post form-->
        <form class="postForm" action="{{{ url("editPostForm") }}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="id" value="{{{ $post->Id }}}"> 
          <input type="hidden" name="name" size="30" value="{{{ $post->User_name }}}"/><br>
          <b>Title:</b>   <br>
         	 <input class="form-control" type="text" name="title" size="40" value="{{{ $post->Title }}}"/><br>
          <b>Image:</b>
         	 <input type="file" name="fileToUpload" id="fileToUpload"/> <br>
          <b>Message:</b> <br>
          	<textarea class="form-control" rows='3' cols='74' name="message">{{{ $post->Message }}}</textarea> <br>
        	<div class="formButtons">
            <input class="btn btn-default" type="submit" value="Save" name="submit" style="text-align: center;">
            <a href="{{{ secure_url('/') }}}">
              <button class="btn btn-default" type="button">Cancel</button>
            </a>
          </div>
        </form>
      </div>
      
    <!--Col 2 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="images/ads/heinz.jpg"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="images/ads/smoking.jpg"></img>
        </div>
      </div>
      
    </div>
@stop