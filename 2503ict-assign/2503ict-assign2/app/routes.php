<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::get('user/search', array('as' => 'user.search', 'uses' => 'UserController@search'));

Route::resource('user', 'UserController');
Route::resource('post', 'PostController');
Route::resource('comment', 'CommentController');
Route::resource('friends', 'FriendController');


//-- Home page
Route::get('/', array('as'=>'home', 'uses'=> 'UserController@index'));


//-- View site documentation
Route::get('site_documentation', function()
{
	return View::make('doc/documentation');
});


/*
//-- Edit a post
Route::get('edit_post/{Id}', function($Id)
{
    $post = get_post($Id);
	return View::make('editPost')->withPost($post);
});

//-- View comments (Has errors)
Route::get('view_comments/{Id}', function($Id)
{
    $post = get_post($Id);
    $comments = get_comments($Id);
	
	if ($post) 
    {
        return View::make('viewComments')->withPost($post)->withComments($comments);
    } 
    else
    {
        die("Error viewing comments {no post}");
    }
});

//-- View site documentation
Route::get('site_documentation', function()
{
	return View::make('doc/documentation');
});


//-- Delete post
Route::get('del_post/{Id}', function($Id)
{   
    delete_post($Id);
	return Redirect::to(secure_url("/"));
});

//-- Delete comment
Route::get('del_comment/{Id}/{pId}', function($Id, $pId)
{   
    delete_comment($Id);
	return Redirect::back();
});

//-- Create post form 
Route::post('createPost', function()
{
    $name = Input::get('name');
    $title = Input::get('title');
    $message = Input::get('message');

    // If successfully created then display newly created item
    if ($name && $title && $message) 
    {
        $id = add_post($name, $title, $message);
        return Redirect::back();
    } 
    else
    {
        die("Error adding post");
    }
});

//-- Edit post form submitted
Route::post('editPostForm', function()
{
    $id = Input::get('id');
    $name = Input::get('name');
    $title = Input::get('title');
    $message = Input::get('message');
    
    if($name && $title && $message){
        update_post($id, $name, $title, $message);
    }
    
    $post = get_post($id);
    $comments = get_comments($id);
    // return Redirect::to( url("view_comments/$id") );
    return View::make('viewComments')->withPost($post)->withComments($comments);
});

//-- Comment form submitted
Route::post('createComment', function()
{
    $pId = Input::get('id');
    $name = Input::get('name');
    $message = Input::get('comment');
    $id = add_comment($pId, $name, $message);
    
    if ($id) 
    {
        // return Redirect::back();
        return Redirect::to("view_comments/$pId");
    } 
    else
    {
        die("Error adding comment");
    }
});





//-- Functions

//-- Gets all posts with a count of its comment
function get_posts_with_comments (){
    $sql = "SELECT Post.Id, Post.Title, Post.Message, Post.User_name, COUNT(Comment.Post_ID) as Comment_count
    FROM Post
    LEFT OUTER JOIN Comment 
    ON Post.Id = Comment.Post_ID
    GROUP BY Post.Id";
    
    $results = DB::select($sql);
    
    return $results;
}

//-- Gets all posts
function get_all_posts ()
{
    $sql = "select * from Post";
    $results = DB::select($sql);
    
    return $results;
}

//-- Get single post
function get_post($Id)
{
    $sql = "SELECT Post.Id, Post.Title, Post.Message, Post.User_name, COUNT(Comment.Post_ID) as Comment_count
    FROM Post
    LEFT OUTER JOIN Comment 
    ON Post.Id = Comment.Post_ID
    WHERE Post.Id = ?";
    
    $posts = DB::select($sql, array($Id));

	// If we get more than one post or none display an error
	if (count($posts) != 1) {
    die("Invalid query or result: $Id \n");
    }

    $post = $posts[0];
	return $post;
}

//-- Add post to database
function add_post($name, $title, $message) 
{
      $sql = "insert into Post (User_name, Title, Message) values (?, ?, ?)";
    
      DB::insert($sql, array($name, $title, $message));
    
      $id = DB::getPdo()->lastInsertId();
    
      return $id;
}

//-- Add comment to database
function add_comment ($pId, $name, $message)
{
    $sql = "insert into Comment (Message, User_name, Post_ID) values (?, ?, ?)";
    
    DB::insert($sql, array($message, $name, $pId));
    
    $id = DB::getPdo()->lastInsertId();
    
    return $id;
}

//-- Get all comments for a post 
function get_comments ($id)
{
    $sql = "select * from Comment where Post_ID = ?";
    $comments = DB::select ($sql, array($id));
    
    return $comments;
}

//-- Delete post
function delete_post($id)
{
    $sql1 = "delete from Comment where Post_ID = ?";
    DB::delete($sql1, array($id));
    
    $sql2 = "delete from Post where Id = ?";
    DB::delete($sql2, array($id));
}

//-- Delete comment
function delete_comment($id)
{
    $sql = "delete from Comment where Id = ?";
    DB::delete($sql, array($id));
}

//-- Edit/update a post
function update_post($id, $name, $title, $message)
{
    $sql = "update Post set Title = ?, Message = ? where id = ?";
    DB::update($sql, array($title, $message, $id));

    $post = get_post($id);
    return $post;
}
*/