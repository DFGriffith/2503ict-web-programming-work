@extends('layouts.master')


@section('title')
	Social Network - Comments
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-8'>
        <!--Post-->
        <div class="comPost">
          {{-- */ $u = User::find($post->user_id) /* --}}
          <img class='photo' src="{{ asset($u->image->url('thumb')) }}">
          <!--<b>Name:</b> {{{ $post->User_name }}} <br> -->
          <b>Title:</b> {{{ $post->title }}} <br> 
          <b>Message:</b> {{{ $post->message }}} <br>
          <b>Privacy:</b> {{{ $post->privacy }}} <br>
          <!--<b>Number of comments:</b> {{{ $post->Comment_count }}}<br>-->
        </div>
        
        <!--Comment form-->
        @if (Auth::check())
          <div class='commentForm'>
            {{ Form::open(array('action' => 'CommentController@store')) }} 
          
            {{ Form::hidden('post_id', $post->id) }} 
            {{ Form::hidden('user_id', Auth::user()->id) }} 
            {{ Form::label('message', 'Message: ') }} 
            {{ Form::text('message') }}
            {{ $errors->first('message') }}
            <p></p>
            {{ Form::submit('Comment') }} 
            {{ Form::close() }}
          </div>
        @endif
        
        <!--Comment timeline-->
        <div class="commentTimeline">
          
          @if (count($comments) == 0)
          <p>You have no Comments!</p>
          
          @else 
          <h4 class="comment">Comments</h4>
          @foreach($comments as $comment)
            <div class="comment">
              {{-- */ $u = User::find($comment->user_id) /* --}}
              <img class='photo' src="{{ asset($u->image->url('thumb')) }}">
              <b>{{{ User::find($comment->user_id)->full_name }}}</b> <br> 
              <b>Message:</b> {{{ $comment->message }}} <br>
              <b>Created:</b> {{{ $comment->created_at }}} <br>
              @if (Auth::check() && $comment->user_id == Auth::user()->id)
                <p></p>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('comment.destroy', $comment->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }} 
                {{ Form::close() }}
              @endif
            </div>
          @endforeach
          @endif
        </div>
        {{ $comments->links() }}
        
      </div>
      
      
    <!--Col 2 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/heinz.jpg')}}"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/smoking.jpg')}}"></img>
        </div>
      </div>
      
    </div>
@stop