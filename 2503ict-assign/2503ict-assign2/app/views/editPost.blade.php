@extends('layouts.master')


@section('title')
	Social Network - Edit Post
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-8'>
        <!--Edit Post form-->
        <div class="postForm">
          {{ Form::model( $post, array('method' => 'PUT', 'route' => array('post.update', $post->id )) ) }}
            {{ Form::label('title', 'Post Title: ') }}
            {{ Form::text('title') }}
            {{ $errors->first('title') }}
            <p></p>
            {{ Form::label('message', 'Message: ') }} 
            {{ Form::text('message') }}
            {{ $errors->first('message') }}
            <p></p>
            {{ Form::label('privacy', 'Privacy Setting:') }}
            {{ Form::select('privacy', array('private' => 'Private', 'public' => 'Public', 'friends' => 'Friends'), 'public'); }}
            <p></p>
            {{ Form::submit('Update') }} 
          {{ Form::close() }}
        </div>
        
      </div>
      
    <!--Col 2 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/heinz.jpg')}}"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/smoking.jpg')}}"></img>
        </div>
      </div>
      
    </div>
@stop