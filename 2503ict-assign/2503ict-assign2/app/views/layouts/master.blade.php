<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title Yeild -->
    <title>@yield('title')</title>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    {{ HTML::style('css/styles.css') }}
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/styles.css">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  </head>

  <body>
    <div class="container">
      
      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            @if (Auth::check())
              <a class="navbar-brand" href="{{{ action('user.show', array( Auth::user()->id )) }}}">Social Network</a>
            @else
              <a class="navbar-brand" href="{{{ secure_url("/") }}}">Social Network</a>
            @endif
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            
            <ul class="nav navbar-nav navbar-right">
              @if (Auth::check())
                <li><a href="{{{ action('user.show', array( Auth::user()->id )) }}}">Home</a></li>
                <li><a href="{{{ action('FriendController@show', array( Auth::user()->id )) }}}">Friends</a></li>
                <li><a href="{{{ action('UserController@logout') }}}">Logout</a></li>
              @else
                <li><a href="{{{ secure_url("/") }}}">Home</a></li>
                <li><a href="{{{ secure_url("/") }}}">Login</a></li>
              @endif
              <li><a href=" {{{ url("site_documentation") }}}">Site Documentation</a></li>
              <li>
                <div class="searchBar" >
                  {{ Form::open( array( 'method' => 'GET', 'route' => array('user.search')) ) }} 
                    {{ Form::text('search', 'Search') }}
                    {{ Form::submit('Search') }} 
                  {{ Form::close() }}
                </div>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    
      <!-- Main Body -->
      @section('content')
      
        <h1>Something went wrong, try returning to home page</h1>
        
      @show

    </div>
    <!-- End container -->
  </body>
</html>