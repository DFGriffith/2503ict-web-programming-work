@extends('layouts.master')


@section('title')
	Social Network - Documentation
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-12'>
        <div class='docBody'>
          <h3>Site Documentation:</h3>
          <p>Although I am quite happy with what I have achieved, I did run into a little trouble with 
          the edit user form. The problem arises when trying to update a users information, the validation 
          rule specifying the email attribute is unique stops the form from updating the user</p>
          <p>I'd a bit of googling and couldn't find much help, but I wasn't to worried I knew if I spent some 
          time (which I planned to do over the next couple days) I could fix the issue. That was until I realised 
          at midnight sunday the 31st of May that the submission was due by 9am. Luckly I completed the majority 
          of the assignment a couple weeks ago and only left a few minor tasks. </p>
        
          <h3>Site Diagram:</h3> 
          <img id='docImg1' src='doc/siteDiagram.png' alt='Site Diagram'> <br>
          
          <h3>Entity-Relationship Diagram:</h3>
          <img id='docImg2' src='doc/erd.png' alt='Database ER Diagram'>
        </div>
      </div>
      
  </div>
@stop