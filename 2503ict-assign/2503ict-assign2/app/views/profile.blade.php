@extends('layouts.master')


@section('title')
	Social Network - Profile
@stop


@section('content')
	<div class='row'>
      <!--Col 1-->
      <div class='col-xs-8'>
        
        <!--User information-->
        <div class = "postForm">
          <h4> {{{ $user->full_name}}}'s information</h4>
          <img class='photo' src="{{ asset($user->image->url('thumb')) }}">
          
          <b>Name:</b> {{{ $user->full_name }}} <br> 
          <b>Age:</b> 
          {{-- */
              $oDateNow = new DateTime();
              $oDateBirth = new DateTime($user->date_of_birth);
              $oDateIntervall = $oDateNow->diff($oDateBirth); 
              echo $oDateIntervall->y;
          /* --}}
          <br>
          <b>Date Joined: </b> {{{ $user->created_at }}} <br> 
          {{ link_to_route('friends.show', 'View friends', array($user->id)) }}
          <p></p>
          
          <!--Check the profile user and logged in users relationship-->
          @if (Auth::check() && $user->id == Auth::user()->id)
            {{ link_to_route('user.edit', 'Edit Profile Information', array( $user->id )) }} 
          @elseif (Auth::check())
            {{-- */ $authUser = User::find(Auth::user()->id); 
                    $friends = $authUser->friend;
                    $truefalse = false;
                    foreach ($friends as $friend){
                      if($friend->friend_id == $user->id){
                        $truefalse = true;
                        $friendId = $friend->id;
                      }
                    }
            /* --}}
            @if ( $truefalse )
              {{ Form::open(array('method' => 'DELETE', 'route' => array('friends.destroy', $friendId))) }}
              {{ Form::hidden('user_id', Auth::user()->id) }} 
              {{ Form::hidden('friend_id', $user->id) }}
              {{ Form::submit('Remove Freind', array('class' => 'btn btn-primary')) }} 
              {{ Form::close() }}
            @else
              {{ Form::open( array( 'action' => 'FriendController@store') ) }}
              {{ Form::hidden('user_id', Auth::user()->id) }} 
              {{ Form::hidden('friend_id', $user->id) }}
              {{ Form::submit('Add Freind', array('class' => 'btn btn-primary')) }} 
              {{ Form::close() }}
            @endif
          @endif
        </div>
        
        <!--Post form-->
        <div class='postForm'>
          @if (Auth::check() && Auth::user()->id == $user->id)
            {{ Form::open(array('action' => 'PostController@store')) }}
              
              {{ Form::hidden('user_id', $user->id) }}
              
              {{ Form::label('title', 'Post Title: ') }}
              {{ Form::text('title') }}
              {{ $errors->first('title') }}
              <p></p>
              {{ Form::label('message', 'Message: ') }} 
              {{ Form::text('message') }}
              {{ $errors->first('message') }}
              <p></p>
              {{ Form::label('privacy', 'Privacy Setting:') }}
              {{ Form::select('privacy', array('private' => 'Private', 'public' => 'Public', 'friends' => 'Friends'), 'public'); }}
              <p></p>
              {{ Form::submit('Post') }} 
            {{ Form::close() }}
          @else
          <h4>Post Timeline </h4>
          @endif
        </div>
        
        
        <!--Post timeline-->
        <div class="postTimeline">
          
          @if (count($posts) == 0)
          <p>No posts!</p>
          
          @else 
          @foreach( $posts as $post)
            {{-- */ $ok = 'false'; /* --}}
            @if ($post->privacy == 'public')
                {{-- */ $ok = 'true' /* --}}
            @elseif ($post->privacy == 'private' && Auth::check() && Auth::user()->id == $user->id)
                {{-- */ $ok = 'true' /* --}}
            @elseif ($post->privacy == 'friends' && Auth::check() && $truefalse)
                 {{-- */ $ok = 'true' /* --}}
            @endif
            
            @if ($ok == 'true')
              <div class="post">
                <img class='photo' src="{{ asset($user->image->url('thumb')) }}">
                <b>Title:</b> {{{ $post->title }}} <br> 
                <b>Message:</b> {{{ $post->message }}} <br>
                <b>Privacy:</b> {{{ $post->privacy }}} <br>
                <b>Created:</b> {{{ $post->created_at }}} <br>
                {{ link_to_route('post.show', 'View Comments', array( $post->id )) }} <br>
                @if (Auth::check() && $user->id == Auth::user()->id)
                  {{ link_to_route('post.edit', 'Edit', array( $post->id )) }} 
                  <p></p>
                  {{ Form::open(array('method' => 'DELETE', 'route' => array('post.destroy', $post->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }} 
                  {{ Form::close() }}
                @endif
              </div>
            @endif
          @endforeach

          @endif
        </div>
      </div>
      
      
      <!--Col 2 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/heinz.jpg')}}"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/smoking.jpg')}}"></img>
        </div>
      </div>
      
    </div>
@stop