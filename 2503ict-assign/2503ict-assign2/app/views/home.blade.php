@extends('layouts.master')


@section('title')
	Social Network - Home
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-3'>
        
        <!--Login form-->
        <div class="postForm">
          <h3>Login</h3><br>
          @if (Auth::check())
            {{ Redirect::route('user.show', Auth::user()->id) }}
          @else
            {{ Form::open( array('url' => secure_url('user/login')) ) }}
              {{ Form::label('email', 'Email Address: ') }}
              {{ Form::email('email') }}
              {{ $errors->first('email') }}
              <p></p>
              {{ Form::label('password', 'Password: ') }} 
              {{ Form::password('password') }}
              {{ $errors->first('password') }}
              <p></p>
              {{ Form::submit('Login') }} 
            {{ Form::close() }}
          @endif
        </div>

      </div>

      
    <!--Col 2 register form-->
      <div class='col-xs-5'>
        
        <!--Registration form-->
        <div class="postForm">
          <h3>Sign up</h3><br>
          {{ Form::open( array('url' => secure_url('user'), 'files'=> true ) ) }}
            {{ Form::label('full_name', 'Full Name: ') }} 
            {{ Form::text('full_name') }}
            {{ $errors->first('full_name') }}
            <p></p>
            {{ Form::label('date_of_birth', 'Date Of Birth: ') }} 
            {{ Form::text('date_of_birth') }}
            {{ $errors->first('date_of_birth') }}
            <p></p>
            {{ Form::label('image', 'Profile Image: ') }} 
            {{ Form::file('image') }}
            {{ $errors->first('image') }}
            <p></p>
            {{ Form::label('email', 'Email Address: ') }} 
            {{ Form::email('email', 'example@gmail.com') }}
            {{ $errors->first('email') }}
            <p></p>
            {{ Form::label('password', 'Password: ') }} 
            {{ Form::password('password') }}
            {{ $errors->first('password') }}
            <p></p>
            {{ Form::label('password_confirmation', 'Confirm Password: ') }} 
            {{ Form::password('password_confirmation') }}
            {{ $errors->first('password_confirmation') }}
            <p></p>
            {{ Form::submit('Sign up') }} 
          {{ Form::close() }}
        </div>
        
      </div>
      
      
      <!--Col 3 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/heinz.jpg')}}"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('/images/ads/smoking.jpg')}}"></img>
        </div>
      </div>
      
  </div>
@stop