@extends('layouts.master')


@section('title')
	Social Network - Edit User Information
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-8'>
        <!--Edit user form-->
        <div class="postForm">
          {{ Form::model( $user, array('method' => 'PUT', 'route' => array('user.update', $user->id )) ) }}
            <!--{{ Form::hidden('email', $user->email) }}-->
            {{ Form::label('full_name', 'Name: ') }}
            {{ Form::text('full_name') }}
            {{ $errors->first('full_name') }}
            <p></p>
            {{ Form::label('date_of_birth', 'Date of birth: ') }} 
            {{ Form::text('date_of_birth') }}
            {{ $errors->first('date_of_birth') }}
            <p></p>
            {{ Form::label('image', 'Profile Image: ') }} 
            {{ Form::file('image') }}
            {{ $errors->first('image') }}
            <p></p>
            {{ Form::label('password', 'Password: ') }} 
            {{ Form::password('password') }}
            {{ $errors->first('password') }}
            <p></p>
            {{ Form::label('password_confirmation', 'Confirm Password: ') }} 
            {{ Form::password('password_confirmation') }}
            {{ $errors->first('password_confirmation') }}
            <p></p>
            {{ Form::label('email', 'Email Address: ') }} 
            {{ Form::email('email') }}
            {{ $errors->first('email') }}
            <P></P>
            {{ Form::submit('Update') }} 
          {{ Form::close() }}
        </div>
        
      </div>
      
    <!--Col 2 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/heinz.jpg')}}"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/smoking.jpg')}}"></img>
        </div>
      </div>
      
    </div>
@stop