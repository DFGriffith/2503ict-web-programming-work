@extends('layouts.master')


@section('title')
	Social Network - Search
@stop


@section('content')
	<div class='row'>
    <!--Col 1-->
      <div class='col-xs-8'>
        
        <!--User List-->
        <div class="commentTimeline">
          
          @if (count($users) == 0)
          <p>No users found!</p>
          
          @else 
          @foreach($users as $user)
            <div class="comment">
              {{-- */ $u = User::find($user->id); /* --}}
              <img class='photo' src="{{ asset($u->image->url('thumb')) }}">
              <b>Name:</b> {{{ $u->full_name }}} <br> 
              {{ link_to_route('user.show', 'View Profile', array( $u->id )) }}
            </div>
          @endforeach
          @endif
        </div>
        
      </div>
      
    <!--Col 2 ads-->
      <div class='col-xs-4'>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/heinz.jpg')}}"></img>
        </div>
        <div class="adcontainer">
          <img class="adimg" src="{{asset('images/ads/smoking.jpg')}}"></img>
        </div>
      </div>
      
    </div>
@stop