<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$v = Validator::make($input, Post::$rules);
		
		if ($v->passes())
		{
			$post = new Post(); 
			$post->title = $input['title']; 
			$post->message = $input['message'];
			$post->user_id = $input['user_id'];
			$post->privacy = $input['privacy'];
			$post->save();
	
			return Redirect::route('post.show', $post->id);
		}
		else
		{
      		return Redirect::route('post.index')->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::find($id);
		$comments = Comment::whereRaw('post_id = ?', array($id))->paginate(8);
		return View::make('viewPost')->withPost($post)->withComments($comments);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);
		return View::make('editPost')->withPost($post);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post = Post::find($id);
   		$input = Input::all();
   		
   		$v = Validator::make($input, Post::$rules);
		
		if ($v->passes()){
   			$post->title = $input['title'];
   			$post->message = $input['message'];
   			$post->privacy = $input['privacy'];
   			$post->save();
		
			return Redirect::route('post.show', $post->id);
		}
		else
		{
			// Show validation errors
      		return Redirect::action('PostController@edit', $product->id)->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id);
		$comments = Comment::whereRaw('post_id = ?', array($id))->get();
		foreach ($comments as $comment){
			$comment->delete();	
		}
   		$post->delete();
		
		return Redirect::back();
	}


}
