<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('home');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$v = Validator::make($input, User::$rules);
		
		if ($v->passes())
		{
			$password = $input['password'];
			$encrypted = Hash::make($password);
			
			$user = new User(); 
			$user->full_name = $input['full_name']; 
			$user->date_of_birth = $input['date_of_birth'];
			$user->email = $input['email']; 
			$user->password = $encrypted;
			if (Input::hasFile('image')) {
				$user->image = $input['image'];
			}
			$user->save();
	
	
			
			$userdata = array(
				'email' => Input::get('email'),
				'password' => Input::get('password')
			);
		
			// authenticate
			if(Auth::attempt($userdata)) {
				// $user = User::find($userdata->email);
				return Redirect::route('user.show', Auth::id());  // email???
			}
			else {
				return Redirect::to(URL::previous())->withInput();
			}
	
		}
		else
		{
      		return Redirect::back()->withErrors($v);
		}
		
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		$posts = Post::whereRaw('user_id = ?', array($id))->orderBy('created_at', 'DESC')->get();
		return View::make('profile')->withPosts($posts)->withUser($user);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		return View::make('editUser')->withUser($user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::find($id);
		$input = Input::all();
		
		$v = Validator::make($input, User::$rules);
		
		if ($v->passes())
		{
			$password = $input['password'];
			$encrypted = Hash::make($password);
			
			$user->full_name = $input['full_name']; 
			$user->date_of_birth = $input['date_of_birth'];
			// $user->email = $input['email']; 
			$user->password = $encrypted;
			$user->save();
	
			return Redirect::route('user.show', $user->id);
		}
		else
		{
      		return Redirect::back()->withErrors($v);
		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	//-- User login
	public function login()
	{
		$userdata = array(
			'email' => Input::get('email'),
			'password' => Input::get('password')
		);
		
		// authenticate
		if(Auth::attempt($userdata)) {
			// $user = User::find($userdata->email);
			return Redirect::route('user.show', Auth::id());  // email???
		}
		else {
			return Redirect::to(URL::previous())->withInput();
		}
		
	}
	
	
	//-- User logout
	public function logout() 
	{
		Auth::logout();
		
		return Redirect::route('user.index');
	}
	
	
	//-- User search function (searches by name)
	public function search() 
	{
		$q = Input::get('search');

	    $searchTerms = explode(' ', $q);

	    $query = DB::table('users');

	    foreach($searchTerms as $term)
    	{
        	$query->where('full_name', 'LIKE', '%'. $term .'%');
	    }

    	$results = $query->get();
		
		return View::make('search')->withUsers($results);
	}


}
