<?php
class Comment extends Eloquent 
{
    public static $rules = array( 
        'message' => 'required|max:150'
    );
    
    function user() 
    {
      return $this->belongsTo('Users');
    }
    
    function post()
    {
        return $this->belongsTo('Posts');
    }
    
}