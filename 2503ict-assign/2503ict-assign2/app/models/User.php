<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;


class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface 
{

	use UserTrait, RemindableTrait, EloquentTrait;

  public function __construct(array $attributes = array()) 
  { 
    $this->hasAttachedFile('image', [
        'styles' => [
            'medium' => '300x300', 
            'thumb' => '100x100'
        ],
        'default_url' => '/images/default-image.png'
    ]);
      
    parent::__construct($attributes);
  }

  
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
  
  
    public static $rules = array( 
        'email' => 'required|email|unique:users',
        'full_name' => 'required|max:50',
        'date_of_birth' => 'required|date',
        'password' => 'required|min:4|confirmed',
        'image' => 'image'
    );
  
    function post() 
    {
      return $this->hasMany('Post');
    }
    
    function comment() 
    {
      return $this->hasMany('Comment');
    }    
    
    function friend() 
    {
      return $this->hasMany('Friend');
    }
    
}
