<?php
class Post extends Eloquent 
{
    public static $rules = array( 
        'title' => 'required|max:20',
        'message' => 'required|max:150', 
    );
}