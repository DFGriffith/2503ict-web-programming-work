<?php

class UserTableSeeder extends Seeder { public function run()
{
      $password = '1234';
	$encrypted = Hash::make($password);
      
      //== user 1
      $user = new User;
      $user->full_name = 'Nikola Tesla';
      $user->email = 'tesla@gmail.com';
      $user->date_of_birth = '10-07-1996';
      $user->password = $encrypted;
      $user->save();
      
      //-- user 2
      $user = new User;
      $user->full_name = 'Albert Einstein';
      $user->email = 'al@gmail.com';
      $user->date_of_birth = '1979-03-03';
      $user->password = $encrypted;
      $user->save();

      //-- user 3
      $user = new User;
      $user->full_name = 'Isaac Newton';
      $user->email = 'ike@gmail.com';
      $user->date_of_birth = '1993-04-01';
      $user->password = $encrypted;
      $user->save();
      
      //-- user 4
      $user = new User;
      $user->full_name = 'Genghis Khan';
      $user->email = 'khan@gmail.com';
      $user->date_of_birth = '2002-10-10';
      $user->password = $encrypted;
      $user->save();
      
      //-- user 5
      $user = new User;
      $user->full_name = 'Jesus';
      $user->email = 'jman@gmail.com';
      $user->date_of_birth = '1993-06-06';
      $user->password = $encrypted;
      $user->save();
      
      //-- user 6
      $user = new User;
      $user->full_name = 'Buddha';
      $user->email = 'buddha@gmail.com';
      $user->date_of_birth = '2014-04-02';
      $user->password = $encrypted;
      $user->save();
      
      //-- user 7
      $user = new User;
      $user->full_name = 'Muhammad';
      $user->email = 'muhammad@gmail.com';
      $user->date_of_birth = '2000-05-12';
      $user->password = $encrypted;
      $user->save();
      
      //-- user 8
      $user = new User;
      $user->full_name = 'Plato';
      $user->email = 'plato@gmail.com';
      $user->date_of_birth = '2010-01-01';
      $user->password = $encrypted;
      $user->save();
      
      //-- user 9
      $user = new User;
      $user->full_name = 'Pythagoras';
      $user->email = 'pythag@gmail.com';
      $user->date_of_birth = '1980-01-01';
      $user->password = $encrypted;
      $user->save();
      
      //-- user 10
      $user = new User;
      $user->full_name = 'Socrates';
      $user->email = 'crates@gmail.com';
      $user->date_of_birth = '2002-01-01';
      $user->password = $encrypted;
      $user->save();
} }