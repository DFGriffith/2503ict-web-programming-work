<?php
class FriendTableSeeder extends Seeder { public function run()
{
      //-- User 1 friends
      $friend = new Friend;
      $friend->user_id = '1';
      $friend->friend_id = '2';
      $friend->save();
      
      $friend = new Friend;
      $friend->user_id = '1';
      $friend->friend_id = '3';
      $friend->save();
      
      //-- User 2 friends
      $friend = new Friend;
      $friend->user_id = '2';
      $friend->friend_id = '4';
      $friend->save();
      
      $friend = new Friend;
      $friend->user_id = '2';
      $friend->friend_id = '1';
      $friend->save();
      
} }