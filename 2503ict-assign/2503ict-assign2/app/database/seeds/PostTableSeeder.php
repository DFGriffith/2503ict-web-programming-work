<?php
class PostTableSeeder extends Seeder { public function run()
{
      //-- Post 1
      $post = new Post;
      $post->title = 'P1';
      $post->message = 'Post 1';
      $post->user_id = 1;
      $post->privacy = 'public';
      $post->save();
      
      //-- Post 2
      $post = new Post;
      $post->title = 'P2';
      $post->message = 'Post 2';
      $post->user_id = 1;
      $post->privacy = 'private';
      $post->save();
      
      //-- Post 3
      $post = new Post;
      $post->title = 'P3';
      $post->message = 'Post 3';
      $post->user_id = 1;
      $post->privacy = 'friends';
      $post->save();
      
      //-- Post 4
      $post = new Post;
      $post->title = 'P4';
      $post->message = 'Post 4';
      $post->user_id = 1;
      $post->privacy = 'public';
      $post->save();
      
      //-- Post 5
      $post = new Post;
      $post->title = 'P5';
      $post->message = 'Post 5';
      $post->user_id = 1;
      $post->privacy = 'public';
      $post->save();
      
      //-- Post 6
      $post = new Post;
      $post->title = 'P6';
      $post->message = 'Post 6';
      $post->user_id = 1;
      $post->privacy = 'friends';
      $post->save();
      
      //-- Post 7
      $post = new Post;
      $post->title = 'P7';
      $post->message = 'Post 7';
      $post->user_id = 1;
      $post->privacy = 'public';
      $post->save();
      
      //-- Post 8
      $post = new Post;
      $post->title = 'P8';
      $post->message = 'Post 8';
      $post->user_id = 1;
      $post->privacy = 'public';
      $post->save();
      
      //-- Post 9
      $post = new Post;
      $post->title = 'P9';
      $post->message = 'Post 9';
      $post->user_id = 1;
      $post->privacy = 'public';
      $post->save();
      
      //-- Post 10
      $post = new Post;
      $post->title = 'P10';
      $post->message = 'Post 10';
      $post->user_id = 1;
      $post->privacy = 'public';
      $post->save();
} }