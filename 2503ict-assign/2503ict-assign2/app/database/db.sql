drop table if exists Post;
drop table if exists Comment;

create table Post (
    Id integer primary key autoincrement,
    Title varchar(40) not null,
    Message varchar(200) not null,
    User_name varchar(20) not null
);

create table Comment (
    Id integer primary key autoincrement,
    Message varchar(200) not null,
    User_name varchar(20) not null,
    Post_ID interger,
    FOREIGN KEY(Post_ID) REFERENCES Post(Id)
);

-- Add default posts
insert into Post( Title, Message, User_name) 
values ('First post','This is the first post','Mike');
insert into Post( Title, Message, User_name) 
values ('Second post','This is the second post','Ted');
insert into Post( Title, Message, User_name) 
values ('Third post','This is the Third post','Sarah');

-- Add default comments for post 1
insert into comment (Message, User_name, Post_ID)
values ('This is the first comment for post 1', 'Ted', '1');
insert into comment (Message, User_name, Post_ID)
values ('This is the second comment for post 1', 'Sarah', '1');
-- Add default comments for post 2
insert into comment (Message, User_name, Post_ID)
values ('This is the first comment for post 2', 'Sarah', '2');