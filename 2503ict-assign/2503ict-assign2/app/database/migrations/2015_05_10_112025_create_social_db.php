<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialDb extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//-- Users table
		Schema::create('Users', function($table)
		{
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password')->index();
			$table->string('full_name');
			$table->date('date_of_birth');
			$table->binary('image')->nullable();
			$table->string('remember_token')->nullable();
			$table->timestamps();
		});
		
		//-- Friends table
		Schema::create('Friends', function($table)
		{
			$table->increments('id'); 
			$table->integer('user_id'); 
			$table->integer('friend_id');
          	$table->timestamps();
          	$table->foreign('user_id')->references('id')->on('Users');
			$table->foreign('friend_id')->references('id')->on('Users');
		});
		
		//-- Posts table
		Schema::create('Posts', function($table)
		{
			$table->increments('id'); 
			$table->string('title'); 
			$table->string('message');
			$table->integer('user_id');
			$table->string('privacy');
          	$table->timestamps();
          	$table->foreign('user_id')->references('id')->on('Users');
		});
		
		//-- Comments table
		Schema::create('Comments', function($table)
		{
			$table->increments('id');
			$table->string('message');
			$table->integer('user_id'); 
			$table->integer('post_id');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('Users');
			$table->foreign('post_id')->references('id')->on('Posts');
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Comments');
		Schema::drop('Posts');
		Schema::drop('friends');
		Schema::drop('Users');
	}

}
