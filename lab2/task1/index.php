<?php 
    $posts = array(
      array('date' => '12/12/12', 'message' => 'Live long and prosper0', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper1', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper2', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper3', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper4', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper5', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper6', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper7', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper8', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg'),
      array('date' => '12/12/12', 'message' => 'Live long and prosper9', 'image' => 'images/10401902_716806361751608_3349388545862528078_n.jpg')
    );
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Lab2 - David Farrow</title>


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">


    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>


  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Social Network</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            
            <ul class="nav navbar-nav navbar-right">
              <li><a href="./">Photos</a></li>
              <li><a href="../navbar-static-top/">Friends</a></li>
              <li><a href="../navbar-fixed-top/">Login</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main body -->
      <div class='row'>
        <div class='col-xs-4'>
          <form>
            Name: <br>
            <input type="text" name="name"/> <br>
            Message: <br>
            <textarea rows='4' cols='23'>Enter your post</textarea> <br>
            <button>Post</button>
          </form>
        </div>
        <div class='col-xs-8'>
          <?php 
            $num = rand(1, 10);
            echo "<br>$num<br>"; 
            
            for ($i=0; $i < $num; $i++){
              $image = $posts[$i]['image'];
              $date = $posts[$i]['date'];
              $message = $posts[$i]['message'];
              
              echo "<div class='post'>";
              echo "<img class='photo' src='$image' alt='spock' >"; 
              echo "Date: $date <br> Message: $message";
              echo "</div>";
            }
          ?>
          
        </div>
      </div>

    </div> <!-- /container -->

  </body>
</html>

