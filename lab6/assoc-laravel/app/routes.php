 <?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/users.php";


// Display search form
Route::get('/', function()
{
	return View::make('user.query');
});

// Perform search and display results
Route::get('search', function()
{
  $query = Input::get('query');

  $results = search($query);

	return View::make('user.results')->withPms($results)->withQuery($query);
});


/* Functions for PM database example. */

/* Search sample data for $name or $year or $state from form. */
function search($query) {
	if(!empty($query)){
	$sql = "select * from pms where name like ?
	or start like ? 
	or finish like ?
	or state like ? ";
	
	$result = DB::select($sql, array("%$query%", "%$query%", "%$query%", "%$query%"));
	}
	return $result;
}